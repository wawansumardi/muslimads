export default ({ store }, inject) => {
  inject('helper', {
    // function to convert currency
    currencyConverter(numberFormat) {
      return Intl.NumberFormat(store.state.defaultCurrency.id, {
        currency: store.state.defaultCurrency.code,
        style: 'currency',
        minimumFractionDigits: 0,
      }).format(numberFormat * store.state.defaultCurrency.value)
    },
    dateRange(payload) {
      const start = new Intl.DateTimeFormat('en-CA', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
      }).format(
        new Date(
          new Date(Date.now()).setDate(
            new Date().getDate() +
              (payload.value > 0 ? -payload.value : payload.value)
          )
        )
      )
      const end = store.getters.getToday.substring(0, 10)

      return {
        date: [start, end],
      }
    },
  })
}
