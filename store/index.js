export const state = () => ({
  search: '',
  defaultCurrency: {
    id: 'US',
    code: 'USD',
    value: 1,
  },
  currency: [
    {
      id: 'US',
      code: 'USD',
      value: 1,
    },
    {
      id: 'ID',
      code: 'IDR',
      value: 14000,
    },
    {
      id: 'SG',
      code: 'SGD',
      value: 1.3,
    },
  ],
  today: new Date(Date.now()),
  customDateRange: [],
  dateRange: [
    {
      name: 'Today',
      value: 0,
    },
    {
      name: 'A week ago',
      value: 7,
    },
  ],
  dateRangeIndex: 0,
  deleteConfimation: false,
})

export const getters = {
  getDefaultCurrency(state) {
    return state.defaultCurrency
  },
  getCurrency(state) {
    return state.currency.filter((item) => item.id !== state.defaultCurrency.id)
  },
  getDateRangeText(state) {
    return state.dateRange[state.dateRangeIndex]
  },
  getToday(state) {
    return new Intl.DateTimeFormat('en-CA').format(state.today)
  },
}

export const mutations = {
  setSearch(state, search) {
    state.search = search
  },
  setDefaultCurrency(state, defaultCurrency) {
    state.defaultCurrency = defaultCurrency
  },
  setDateRangeIndex(state, dateRangeIndex) {
    state.dateRangeIndex = dateRangeIndex
  },
  setCustomDateRange(state, customDate) {
    state.customDateRange = customDate.sort((a, b) => new Date(a) - new Date(b))
  },
  saveDateRange(state, dateRange) {
    const customDate = state.dateRange.find(
      (item) => item.value === dateRange.value
    )

    if (customDate) {
      Object.assign(state.dateRange[2], dateRange)
    } else {
      state.dateRange.push(dateRange)
    }
  },
  toggleDelete(state) {
    state.deleteConfimation = !state.deleteConfimation
  },
  assignDateRange(state, dateRange) {
    Object.assign(state.dateRange[state.dateRangeIndex], dateRange)
  },
}

export const actions = {
  converterCurrency({ state }, numberFormat) {
    return numberFormat.toLocaleString(state.defaultCurrency.id, {
      currency: state.defaultCurrency.code,
      style: 'currency',
      minimumFractionDigits: 0,
    })
  },
  pushToDateRange({ state, commit }) {
    const customDate = {
      name: state.customDateRange
        .map((date) => new Date(date).toLocaleDateString())
        .join(' ~ '),
      value: 'customDate',
      date: state.customDateRange,
    }
    commit('saveDateRange', customDate)
    commit('setDateRangeIndex', 2)
  },
  // TODO: get currency from server
  fetchDataCurrencyFromServer() {},
}
