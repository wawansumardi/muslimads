export const state = () => ({
  headers: [
    {
      text: 'No',
      value: 'id',
      groupable: false,
    },
    {
      text: 'AdsTitle',
      value: 'ads[0].name',
      groupable: false,
    },
    {
      text: 'Position',
      value: 'ads[0].position',
      groupable: true,
    },
    {
      text: 'Advertiser',
      value: 'ads[0].users[0].name',
      groupable: true,
    },
    {
      text: 'Views',
      value: 'views',
      groupable: false,
    },
    {
      text: 'Clicks',
      value: 'clicks',
      groupable: false,
    },
    {
      text: 'PublishOn',
      value: 'sites[0].host',
      groupable: true,
    },
    {
      text: 'Path',
      value: 'sites[0].path',
      groupable: false,
    },
    {
      text: 'Created At',
      value: 'createdAt',
      groupable: false,
    },
  ],
  transactions: [
    {
      id: 1,
      ads: [
        {
          id: 9,
          position: 'popup',
          name: 'Trading Places',
          users: [
            {
              id: 3,
              name: 'Hedy Windsor',
            },
          ],
        },
      ],
      sites: [
        {
          id: 2,
          host: 'bizjournals.com',
          path: '/justo/sollicitudin/ut/suscipit/a/feugiat/et.html',
          users: [
            {
              id: 3,
              name: 'Shaylynn Auten',
            },
          ],
        },
      ],
      views: 94,
      clicks: 10,
      createdAt: new Date().toISOString(),
    },
    {
      id: 2,
      ads: [
        {
          id: 4,
          position: 'top',
          name: 'You Only Live Once',
          users: [
            {
              id: 10,
              name: 'Brander Schurcke',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'cnbc.com',
          path: '/justo/maecenas/rhoncus/aliquam/lacus.jsp',
          users: [
            {
              id: 8,
              name: 'Moselle Hutson',
            },
          ],
        },
      ],
      views: 17,
      clicks: 82,
      createdAt: new Date(Date.now()).toISOString(),
    },
    {
      id: 3,
      ads: [
        {
          id: 10,
          position: 'docked',
          name: 'Dave Attell: Captain Miserable',
          users: [
            {
              id: 3,
              name: 'Alix Gillogley',
            },
          ],
        },
      ],
      sites: [
        {
          id: 6,
          host: 'google.com.hk',
          path: '/sit/amet/diam/in/magna.jpg',
          users: [
            {
              id: 8,
              name: 'Susi Packe',
            },
          ],
        },
      ],
      views: 23,
      clicks: 79,
      createdAt: new Date(Date.now()).toISOString(),
    },
    {
      id: 4,
      ads: [
        {
          id: 9,
          position: 'top',
          name: 'Facing the Giants',
          users: [
            {
              id: 6,
              name: 'Findley Melhuish',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'mashable.com',
          path: '/pretium/iaculis/justo/in/hac/habitasse.jsp',
          users: [
            {
              id: 7,
              name: 'Tully Iacofo',
            },
          ],
        },
      ],
      views: 56,
      clicks: 1,
      createdAt: new Date(
        new Date().setDate(new Date().getDate() - 7)
      ).toISOString(),
    },
    {
      id: 5,
      ads: [
        {
          id: 1,
          position: 'docked',
          name: 'My Friend Henry (Ystäväni Henry)',
          users: [
            {
              id: 8,
              name: 'Stephana Carlick',
            },
          ],
        },
      ],
      sites: [
        {
          id: 3,
          host: 'gravatar.com',
          path: '/fusce/consequat/nulla/nisl/nunc/nisl.html',
          users: [
            {
              id: 3,
              name: 'Hyman Deveril',
            },
          ],
        },
      ],
      views: 14,
      clicks: 52,
      createdAt: new Date(
        new Date().setDate(new Date().getDate() - 7)
      ).toISOString(),
    },
    {
      id: 6,
      ads: [
        {
          id: 1,
          position: 'popup',
          name: 'Playing',
          users: [
            {
              id: 4,
              name: 'Gail Romaynes',
            },
          ],
        },
      ],
      sites: [
        {
          id: 1,
          host: 'etsy.com',
          path: '/eget/vulputate/ut/ultrices/vel.js',
          users: [
            {
              id: 5,
              name: 'Theo Reitenbach',
            },
          ],
        },
      ],
      views: 11,
      clicks: 90,
      createdAt: new Date(
        new Date().setDate(new Date().getDate() - 7)
      ).toISOString(),
    },
    {
      id: 7,
      ads: [
        {
          id: 2,
          position: 'left',
          name: 'Stage Fright',
          users: [
            {
              id: 3,
              name: 'Brandy Beardon',
            },
          ],
        },
      ],
      sites: [
        {
          id: 9,
          host: 'deliciousdays.com',
          path: '/lacinia/erat.json',
          users: [
            {
              id: 6,
              name: 'Adelaida Wellsman',
            },
          ],
        },
      ],
      views: 8,
      clicks: 99,
      createdAt: '2022-11-14T04:07:44Z',
    },
    {
      id: 8,
      ads: [
        {
          id: 3,
          position: 'bottom',
          name: 'Malaya',
          users: [
            {
              id: 4,
              name: 'Lemuel Tallow',
            },
          ],
        },
      ],
      sites: [
        {
          id: 2,
          host: 'fotki.com',
          path: '/posuere/cubilia.jsp',
          users: [
            {
              id: 6,
              name: 'Bary Pounder',
            },
          ],
        },
      ],
      views: 27,
      clicks: 99,
      createdAt: '2021-10-27T21:54:01Z',
    },
    {
      id: 9,
      ads: [
        {
          id: 5,
          position: 'docked',
          name: 'Lady in Cement',
          users: [
            {
              id: 2,
              name: 'Alexandros Barkus',
            },
          ],
        },
      ],
      sites: [
        {
          id: 6,
          host: 'multiply.com',
          path: '/tortor/eu.jsp',
          users: [
            {
              id: 2,
              name: 'Janna Lenox',
            },
          ],
        },
      ],
      views: 63,
      clicks: 15,
      createdAt: '2022-07-27T00:52:42Z',
    },
    {
      id: 10,
      ads: [
        {
          id: 1,
          position: 'bottom',
          name: "Twilight of a Woman's Soul (Sumerki zhenskoi dushi)",
          users: [
            {
              id: 6,
              name: 'Tab Addess',
            },
          ],
        },
      ],
      sites: [
        {
          id: 7,
          host: 'bing.com',
          path: '/in/faucibus/orci/luctus/et/ultrices.jsp',
          users: [
            {
              id: 8,
              name: 'Jackquelin McIan',
            },
          ],
        },
      ],
      views: 21,
      clicks: 98,
      createdAt: '2022-07-09T21:08:27Z',
    },
    {
      id: 11,
      ads: [
        {
          id: 8,
          position: 'docked',
          name: 'A Chairy Tale',
          users: [
            {
              id: 9,
              name: 'Jolyn Goddard',
            },
          ],
        },
      ],
      sites: [
        {
          id: 10,
          host: 'constantcontact.com',
          path: '/molestie/hendrerit/at.json',
          users: [
            {
              id: 5,
              name: 'Norri Malicki',
            },
          ],
        },
      ],
      views: 41,
      clicks: 29,
      createdAt: '2022-06-29T22:52:38Z',
    },
    {
      id: 12,
      ads: [
        {
          id: 8,
          position: 'popup',
          name: '7th Dawn, The',
          users: [
            {
              id: 1,
              name: 'Aubrey Tolomelli',
            },
          ],
        },
      ],
      sites: [
        {
          id: 10,
          host: 'naver.com',
          path: '/tristique/fusce/congue/diam/id.jpg',
          users: [
            {
              id: 1,
              name: 'Malva Bradford',
            },
          ],
        },
      ],
      views: 52,
      clicks: 24,
      createdAt: '2022-06-06T20:33:56Z',
    },
    {
      id: 13,
      ads: [
        {
          id: 8,
          position: 'top',
          name: 'Naked Harbour (Vuosaari)',
          users: [
            {
              id: 7,
              name: 'Giselbert Brownsall',
            },
          ],
        },
      ],
      sites: [
        {
          id: 6,
          host: 'wp.com',
          path: '/duis/bibendum/felis.aspx',
          users: [
            {
              id: 10,
              name: 'Welsh Litchfield',
            },
          ],
        },
      ],
      views: 18,
      clicks: 57,
      createdAt: '2022-01-13T19:33:56Z',
    },
    {
      id: 14,
      ads: [
        {
          id: 5,
          position: 'bottom',
          name: 'Heartbreak Kid, The',
          users: [
            {
              id: 4,
              name: 'Parnell Dewitt',
            },
          ],
        },
      ],
      sites: [
        {
          id: 4,
          host: 'yahoo.co.jp',
          path: '/elementum/eu/interdum/eu/tincidunt/in.jpg',
          users: [
            {
              id: 4,
              name: 'Amelina Maffi',
            },
          ],
        },
      ],
      views: 100,
      clicks: 47,
      createdAt: '2022-02-05T17:37:22Z',
    },
    {
      id: 15,
      ads: [
        {
          id: 4,
          position: 'bottom',
          name: 'Celsius 41.11: The Temperature at Which the Brain... Begins to Die',
          users: [
            {
              id: 1,
              name: 'Eleanor Hawk',
            },
          ],
        },
      ],
      sites: [
        {
          id: 2,
          host: 'shop-pro.jp',
          path: '/augue/aliquam/erat/volutpat/in/congue/etiam.js',
          users: [
            {
              id: 1,
              name: 'Westbrook Hegg',
            },
          ],
        },
      ],
      views: 52,
      clicks: 84,
      createdAt: '2021-11-25T18:44:15Z',
    },
    {
      id: 16,
      ads: [
        {
          id: 1,
          position: 'docked',
          name: 'Killer at Large',
          users: [
            {
              id: 2,
              name: 'Tammie Reedick',
            },
          ],
        },
      ],
      sites: [
        {
          id: 2,
          host: 'un.org',
          path: '/donec/semper.jpg',
          users: [
            {
              id: 5,
              name: 'Brett Wabey',
            },
          ],
        },
      ],
      views: 47,
      clicks: 29,
      createdAt: '2022-02-05T11:31:49Z',
    },
    {
      id: 17,
      ads: [
        {
          id: 1,
          position: 'left',
          name: 'She-Devil',
          users: [
            {
              id: 7,
              name: 'Kelcey Bearward',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'cdc.gov',
          path: '/odio/in/hac/habitasse.jpg',
          users: [
            {
              id: 2,
              name: 'Marie-jeanne Pauer',
            },
          ],
        },
      ],
      views: 57,
      clicks: 21,
      createdAt: '2022-09-25T04:26:53Z',
    },
    {
      id: 18,
      ads: [
        {
          id: 3,
          position: 'inview',
          name: 'Ranma ½: Big Trouble in Nekonron, China (Ranma ½: Chûgoku Nekonron daikessen! Okite yaburi no gekitô hen)',
          users: [
            {
              id: 10,
              name: 'Micheal Ramos',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'mail.ru',
          path: '/id.js',
          users: [
            {
              id: 1,
              name: 'Piper Portugal',
            },
          ],
        },
      ],
      views: 3,
      clicks: 80,
      createdAt: '2022-05-25T11:11:27Z',
    },
    {
      id: 19,
      ads: [
        {
          id: 3,
          position: 'inview',
          name: 'Infernal Affairs III (Mou gaan dou III: Jung gik mou gaan)',
          users: [
            {
              id: 5,
              name: 'Orlando Itzak',
            },
          ],
        },
      ],
      sites: [
        {
          id: 6,
          host: 'businesswire.com',
          path: '/et.aspx',
          users: [
            {
              id: 5,
              name: 'Alexis Papaminas',
            },
          ],
        },
      ],
      views: 31,
      clicks: 69,
      createdAt: '2022-05-26T16:29:35Z',
    },
    {
      id: 20,
      ads: [
        {
          id: 5,
          position: 'inview',
          name: 'Out On A Limb',
          users: [
            {
              id: 9,
              name: 'Orsa Wenzel',
            },
          ],
        },
      ],
      sites: [
        {
          id: 8,
          host: 'fda.gov',
          path: '/purus/phasellus/in/felis.xml',
          users: [
            {
              id: 10,
              name: 'Christi McNeigh',
            },
          ],
        },
      ],
      views: 77,
      clicks: 57,
      createdAt: '2021-11-01T22:21:26Z',
    },
    {
      id: 21,
      ads: [
        {
          id: 8,
          position: 'top',
          name: 'Black Cauldron, The',
          users: [
            {
              id: 7,
              name: 'Philippa Westmoreland',
            },
          ],
        },
      ],
      sites: [
        {
          id: 10,
          host: 'bigcartel.com',
          path: '/nisi/vulputate/nonummy/maecenas/tincidunt/lacus/at.jsp',
          users: [
            {
              id: 3,
              name: 'Melva Staines',
            },
          ],
        },
      ],
      views: 43,
      clicks: 50,
      createdAt: '2021-12-06T08:09:21Z',
    },
    {
      id: 22,
      ads: [
        {
          id: 10,
          position: 'inview',
          name: "Everybody's Famous! (Iedereen beroemd!)",
          users: [
            {
              id: 5,
              name: 'Katuscha Hover',
            },
          ],
        },
      ],
      sites: [
        {
          id: 7,
          host: 'boston.com',
          path: '/luctus/cum.js',
          users: [
            {
              id: 2,
              name: 'Norris Gribbon',
            },
          ],
        },
      ],
      views: 69,
      clicks: 47,
      createdAt: '2021-11-02T13:27:54Z',
    },
    {
      id: 23,
      ads: [
        {
          id: 4,
          position: 'right',
          name: 'Without Limits',
          users: [
            {
              id: 8,
              name: 'Janene Jaeggi',
            },
          ],
        },
      ],
      sites: [
        {
          id: 1,
          host: 'ovh.net',
          path: '/mi/sit/amet/lobortis/sapien/sapien/non.xml',
          users: [
            {
              id: 8,
              name: 'Malissa Procter',
            },
          ],
        },
      ],
      views: 26,
      clicks: 44,
      createdAt: '2021-12-21T02:51:08Z',
    },
    {
      id: 24,
      ads: [
        {
          id: 7,
          position: 'popup',
          name: 'Angel in Cracow (Aniol w Krakowie)',
          users: [
            {
              id: 8,
              name: 'Maitilde Stork',
            },
          ],
        },
      ],
      sites: [
        {
          id: 3,
          host: 'xinhuanet.com',
          path: '/lectus.html',
          users: [
            {
              id: 10,
              name: 'Peterus Mains',
            },
          ],
        },
      ],
      views: 19,
      clicks: 50,
      createdAt: '2022-11-05T07:44:46Z',
    },
    {
      id: 25,
      ads: [
        {
          id: 7,
          position: 'inview',
          name: '5th Musketeer, The (a.k.a. Fifth Musketeer, The)',
          users: [
            {
              id: 6,
              name: 'Ethel Davey',
            },
          ],
        },
      ],
      sites: [
        {
          id: 10,
          host: 'pagesperso-orange.fr',
          path: '/integer/non/velit/donec/diam/neque.jpg',
          users: [
            {
              id: 3,
              name: 'Renee Shepard',
            },
          ],
        },
      ],
      views: 54,
      clicks: 50,
      createdAt: '2022-05-10T12:04:42Z',
    },
    {
      id: 26,
      ads: [
        {
          id: 5,
          position: 'left',
          name: 'Bay of Blood (a.k.a. Twitch of the Death Nerve) (Reazione a catena)',
          users: [
            {
              id: 9,
              name: 'Arman Glennon',
            },
          ],
        },
      ],
      sites: [
        {
          id: 9,
          host: 'weather.com',
          path: '/duis/bibendum/felis/sed/interdum/venenatis/turpis.jpg',
          users: [
            {
              id: 8,
              name: 'Ethe Doble',
            },
          ],
        },
      ],
      views: 63,
      clicks: 16,
      createdAt: '2022-05-01T01:49:48Z',
    },
    {
      id: 27,
      ads: [
        {
          id: 9,
          position: 'inview',
          name: 'Arrowsmith',
          users: [
            {
              id: 3,
              name: 'Mariele Raffels',
            },
          ],
        },
      ],
      sites: [
        {
          id: 4,
          host: 'techcrunch.com',
          path: '/ut/erat/curabitur/gravida.js',
          users: [
            {
              id: 3,
              name: 'Brnaba Wellfare',
            },
          ],
        },
      ],
      views: 90,
      clicks: 62,
      createdAt: '2022-01-12T09:00:24Z',
    },
    {
      id: 28,
      ads: [
        {
          id: 7,
          position: 'left',
          name: "Schindler's List",
          users: [
            {
              id: 3,
              name: 'Gorden Bridat',
            },
          ],
        },
      ],
      sites: [
        {
          id: 3,
          host: 'vistaprint.com',
          path: '/lorem.json',
          users: [
            {
              id: 8,
              name: 'Jeno Landrean',
            },
          ],
        },
      ],
      views: 20,
      clicks: 40,
      createdAt: '2022-05-15T07:26:50Z',
    },
    {
      id: 29,
      ads: [
        {
          id: 7,
          position: 'popup',
          name: 'Lady and the Reaper, The (Dama y la muerte, La)',
          users: [
            {
              id: 5,
              name: 'Thomasin McPaike',
            },
          ],
        },
      ],
      sites: [
        {
          id: 3,
          host: 'csmonitor.com',
          path: '/justo/in/blandit.aspx',
          users: [
            {
              id: 5,
              name: 'Merrie Slaten',
            },
          ],
        },
      ],
      views: 9,
      clicks: 63,
      createdAt: '2022-06-26T13:58:54Z',
    },
    {
      id: 30,
      ads: [
        {
          id: 10,
          position: 'top',
          name: 'Prairie Love',
          users: [
            {
              id: 9,
              name: 'Darill Greenstock',
            },
          ],
        },
      ],
      sites: [
        {
          id: 6,
          host: 'sciencedaily.com',
          path: '/quis.js',
          users: [
            {
              id: 7,
              name: 'Marney Drinkwater',
            },
          ],
        },
      ],
      views: 48,
      clicks: 16,
      createdAt: '2021-12-08T11:45:03Z',
    },
    {
      id: 31,
      ads: [
        {
          id: 10,
          position: 'top',
          name: 'Every Little Step',
          users: [
            {
              id: 9,
              name: 'Leda Butchers',
            },
          ],
        },
      ],
      sites: [
        {
          id: 2,
          host: 'istockphoto.com',
          path: '/erat.html',
          users: [
            {
              id: 2,
              name: 'Cordell Alfuso',
            },
          ],
        },
      ],
      views: 13,
      clicks: 94,
      createdAt: '2021-10-01T18:15:27Z',
    },
    {
      id: 32,
      ads: [
        {
          id: 3,
          position: 'bottom',
          name: 'Encino Man',
          users: [
            {
              id: 9,
              name: 'Piper Djokovic',
            },
          ],
        },
      ],
      sites: [
        {
          id: 3,
          host: 'exblog.jp',
          path: '/sit/amet/nunc/viverra.aspx',
          users: [
            {
              id: 9,
              name: 'Frederic Orridge',
            },
          ],
        },
      ],
      views: 70,
      clicks: 19,
      createdAt: '2022-01-07T07:27:28Z',
    },
    {
      id: 33,
      ads: [
        {
          id: 5,
          position: 'popup',
          name: 'Utamaro and His Five Women (Utamaro o meguru gonin no onna)',
          users: [
            {
              id: 7,
              name: 'Zenia Rickson',
            },
          ],
        },
      ],
      sites: [
        {
          id: 4,
          host: 'umich.edu',
          path: '/curabitur.jsp',
          users: [
            {
              id: 10,
              name: 'Cherri Salery',
            },
          ],
        },
      ],
      views: 46,
      clicks: 55,
      createdAt: '2021-11-09T01:53:56Z',
    },
    {
      id: 34,
      ads: [
        {
          id: 8,
          position: 'left',
          name: 'Pianomania',
          users: [
            {
              id: 1,
              name: 'Dyan Boumphrey',
            },
          ],
        },
      ],
      sites: [
        {
          id: 7,
          host: 'amazon.de',
          path: '/fusce/congue.jpg',
          users: [
            {
              id: 2,
              name: 'Velma Bramstom',
            },
          ],
        },
      ],
      views: 13,
      clicks: 79,
      createdAt: '2021-12-07T00:58:04Z',
    },
    {
      id: 35,
      ads: [
        {
          id: 7,
          position: 'right',
          name: "Solomon Northup's Odyssey",
          users: [
            {
              id: 6,
              name: 'Paulette Manlow',
            },
          ],
        },
      ],
      sites: [
        {
          id: 6,
          host: 'dagondesign.com',
          path: '/hac/habitasse.json',
          users: [
            {
              id: 8,
              name: 'Melicent Tyrie',
            },
          ],
        },
      ],
      views: 61,
      clicks: 80,
      createdAt: '2022-06-09T09:17:43Z',
    },
    {
      id: 36,
      ads: [
        {
          id: 5,
          position: 'docked',
          name: 'Triggermen',
          users: [
            {
              id: 10,
              name: 'Jen Bernardt',
            },
          ],
        },
      ],
      sites: [
        {
          id: 2,
          host: 'fda.gov',
          path: '/vestibulum/vestibulum/ante/ipsum/primis/in/faucibus.png',
          users: [
            {
              id: 9,
              name: 'Carmela Botterill',
            },
          ],
        },
      ],
      views: 12,
      clicks: 19,
      createdAt: '2021-10-14T22:52:12Z',
    },
    {
      id: 37,
      ads: [
        {
          id: 6,
          position: 'top',
          name: 'Start the Revolution Without Me',
          users: [
            {
              id: 10,
              name: 'Essa Coburn',
            },
          ],
        },
      ],
      sites: [
        {
          id: 10,
          host: 'telegraph.co.uk',
          path: '/neque/libero.jsp',
          users: [
            {
              id: 10,
              name: 'Haskell Philipeau',
            },
          ],
        },
      ],
      views: 10,
      clicks: 75,
      createdAt: '2021-10-31T13:28:28Z',
    },
    {
      id: 38,
      ads: [
        {
          id: 10,
          position: 'left',
          name: '61*',
          users: [
            {
              id: 7,
              name: 'Leonard Hugo',
            },
          ],
        },
      ],
      sites: [
        {
          id: 7,
          host: 'bandcamp.com',
          path: '/odio/condimentum/id/luctus/nec/molestie/sed.png',
          users: [
            {
              id: 2,
              name: 'Bamby Grendon',
            },
          ],
        },
      ],
      views: 18,
      clicks: 32,
      createdAt: '2022-01-10T03:06:11Z',
    },
    {
      id: 39,
      ads: [
        {
          id: 3,
          position: 'left',
          name: "Gunman's Walk ",
          users: [
            {
              id: 10,
              name: 'Brice Arnaudot',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'boston.com',
          path: '/felis/donec/semper/sapien/a.xml',
          users: [
            {
              id: 8,
              name: 'Philbert Sammes',
            },
          ],
        },
      ],
      views: 62,
      clicks: 95,
      createdAt: '2022-05-17T07:38:34Z',
    },
    {
      id: 40,
      ads: [
        {
          id: 1,
          position: 'top',
          name: 'Painted Veil, The',
          users: [
            {
              id: 4,
              name: 'Lynette Nason',
            },
          ],
        },
      ],
      sites: [
        {
          id: 9,
          host: 'netscape.com',
          path: '/parturient/montes/nascetur/ridiculus/mus.aspx',
          users: [
            {
              id: 7,
              name: 'Marchall Gerrell',
            },
          ],
        },
      ],
      views: 28,
      clicks: 40,
      createdAt: '2022-07-04T11:06:53Z',
    },
    {
      id: 41,
      ads: [
        {
          id: 4,
          position: 'right',
          name: 'Pride of the Yankees, The',
          users: [
            {
              id: 4,
              name: 'Gloria Waby',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'sitemeter.com',
          path: '/pretium/nisl/ut/volutpat/sapien/arcu/sed.js',
          users: [
            {
              id: 6,
              name: 'Cullin Chell',
            },
          ],
        },
      ],
      views: 26,
      clicks: 30,
      createdAt: '2021-12-15T18:15:13Z',
    },
    {
      id: 42,
      ads: [
        {
          id: 8,
          position: 'docked',
          name: 'Doc of the Dead',
          users: [
            {
              id: 4,
              name: 'Sibel Gately',
            },
          ],
        },
      ],
      sites: [
        {
          id: 1,
          host: 'vkontakte.ru',
          path: '/ornare/consequat/lectus.aspx',
          users: [
            {
              id: 1,
              name: 'Daven Rushmare',
            },
          ],
        },
      ],
      views: 29,
      clicks: 28,
      createdAt: '2021-11-23T03:34:14Z',
    },
    {
      id: 43,
      ads: [
        {
          id: 7,
          position: 'popup',
          name: 'Live Wire',
          users: [
            {
              id: 6,
              name: 'Suzanne Boissieux',
            },
          ],
        },
      ],
      sites: [
        {
          id: 7,
          host: 'ebay.co.uk',
          path: '/eros/suspendisse/accumsan/tortor.aspx',
          users: [
            {
              id: 3,
              name: 'Jozef Gumbrell',
            },
          ],
        },
      ],
      views: 15,
      clicks: 67,
      createdAt: '2022-01-07T00:26:19Z',
    },
    {
      id: 44,
      ads: [
        {
          id: 8,
          position: 'left',
          name: 'Creepshow 3',
          users: [
            {
              id: 6,
              name: 'Nancee Milson',
            },
          ],
        },
      ],
      sites: [
        {
          id: 1,
          host: 'i2i.jp',
          path: '/cras/in/purus.xml',
          users: [
            {
              id: 9,
              name: 'Isobel Dahle',
            },
          ],
        },
      ],
      views: 85,
      clicks: 89,
      createdAt: '2022-08-15T02:44:16Z',
    },
    {
      id: 45,
      ads: [
        {
          id: 7,
          position: 'left',
          name: 'Outside Providence',
          users: [
            {
              id: 1,
              name: 'Byrle Skippon',
            },
          ],
        },
      ],
      sites: [
        {
          id: 8,
          host: 'devhub.com',
          path: '/pretium/nisl/ut/volutpat/sapien/arcu/sed.xml',
          users: [
            {
              id: 9,
              name: 'Ula Querree',
            },
          ],
        },
      ],
      views: 71,
      clicks: 66,
      createdAt: '2022-03-06T11:27:31Z',
    },
    {
      id: 46,
      ads: [
        {
          id: 2,
          position: 'popup',
          name: 'Dolls',
          users: [
            {
              id: 10,
              name: 'Emmalynne Dowdeswell',
            },
          ],
        },
      ],
      sites: [
        {
          id: 7,
          host: 'hud.gov',
          path: '/enim/in/tempor/turpis.js',
          users: [
            {
              id: 4,
              name: 'Theo Stott',
            },
          ],
        },
      ],
      views: 98,
      clicks: 20,
      createdAt: '2022-09-12T13:53:22Z',
    },
    {
      id: 47,
      ads: [
        {
          id: 6,
          position: 'inview',
          name: 'Brothers Solomon, The',
          users: [
            {
              id: 2,
              name: 'Bennett Pinkstone',
            },
          ],
        },
      ],
      sites: [
        {
          id: 9,
          host: 'indiegogo.com',
          path: '/luctus/tincidunt/nulla/mollis/molestie/lorem/quisque.png',
          users: [
            {
              id: 1,
              name: 'Lonnie Klejin',
            },
          ],
        },
      ],
      views: 91,
      clicks: 33,
      createdAt: '2022-07-08T23:08:38Z',
    },
    {
      id: 48,
      ads: [
        {
          id: 2,
          position: 'right',
          name: 'Esa ja Vesa - auringonlaskun ratsastajat',
          users: [
            {
              id: 7,
              name: 'Aimil Artz',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'rambler.ru',
          path: '/pellentesque.html',
          users: [
            {
              id: 6,
              name: 'Mariquilla Maren',
            },
          ],
        },
      ],
      views: 30,
      clicks: 51,
      createdAt: '2022-06-28T21:05:47Z',
    },
    {
      id: 49,
      ads: [
        {
          id: 3,
          position: 'inview',
          name: 'Azur & Asmar (Azur et Asmar)',
          users: [
            {
              id: 6,
              name: 'Ted Clac',
            },
          ],
        },
      ],
      sites: [
        {
          id: 5,
          host: 'sun.com',
          path: '/pellentesque.jpg',
          users: [
            {
              id: 4,
              name: 'Guinna Serginson',
            },
          ],
        },
      ],
      views: 62,
      clicks: 73,
      createdAt: '2022-10-10T12:54:53Z',
    },
    {
      id: 50,
      ads: [
        {
          id: 9,
          position: 'docked',
          name: 'Lars and the Real Girl',
          users: [
            {
              id: 5,
              name: 'Devlin Peddie',
            },
          ],
        },
      ],
      sites: [
        {
          id: 1,
          host: 'loc.gov',
          path: '/integer/pede.png',
          users: [
            {
              id: 6,
              name: 'Tiffie Dozdill',
            },
          ],
        },
      ],
      views: 68,
      clicks: 29,
      createdAt: '2021-12-10T14:14:35Z',
    },
  ],
})

export const getters = {
  getAdsTransactions(state, _getters, rooState) {
    return state.transactions.filter(
      (item) =>
        new Date(new Date(item.createdAt).toLocaleDateString()) >=
          new Date(
            new Date(rooState.customDateRange[0]).toLocaleDateString()
          ) &&
        new Date(new Date(item.createdAt).toLocaleDateString()) <=
          new Date(new Date(rooState.customDateRange[1]).toLocaleDateString())
    )
  },
}

export const mutations = {
  setEarningOrSpending(state, earningOrSpending) {
    state.earningOrSpending = earningOrSpending
  },
}

export const actions = {
  // TODO: get data transactions ads from server
  getDataTransactions() {},
}
