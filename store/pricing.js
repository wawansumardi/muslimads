export const state = () => ({
  pricingItems: [
    {
      id: 1,
      name: 'top',
      priceView: 5,
      priceClick: 4,
      publisherFee: 0.1,
      vendorFee: 0.4,
    },
    {
      id: 2,
      name: 'bottom',
      priceView: 2,
      priceClick: 1,
      publisherFee: 0.3,
      vendorFee: 0.2,
    },
    {
      id: 3,
      name: 'left',
      priceView: 2,
      priceClick: 1,
      publisherFee: 0.1,
      vendorFee: 0.5,
    },
    {
      id: 4,
      name: 'right',
      priceView: 1,
      priceClick: 4,
      publisherFee: 0.2,
      vendorFee: 0.2,
    },
    {
      id: 5,
      name: 'inview',
      priceView: 4,
      priceClick: 4,
      publisherFee: 0.1,
      vendorFee: 0.4,
    },
    {
      id: 6,
      name: 'docked',
      priceView: 3,
      priceClick: 5,
      publisherFee: 0.2,
      vendorFee: 0.5,
    },
    {
      id: 7,
      name: 'popup',
      priceView: 3,
      priceClick: 1,
      publisherFee: 0.5,
      vendorFee: 0.4,
    },
  ],
  position: '',
  classification: '',
})

export const getters = {
  getPositions: (state) => state.pricingItems,
}

export const mutations = {
  setPosition(state, position) {
    state.position = position
  },
  setClassification(state, classification) {
    state.classification = classification
  },
}
